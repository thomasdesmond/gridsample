﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace GridDemo
{
    public partial class GridDemoPage : ContentPage
    {
        public GridDemoPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GridDemoPage)}:  constructor");

            InitializeComponent();
        }

        void OnPageSizeChanged(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnPageSizeChanged)}");

            UpdateLabels();
            FirstRowSlider.Value = TopLeftLabel.Height;
        }

        void FirstRowSliderValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(FirstRowSliderValueChanged)}:  {e.NewValue}");

            OuterLayoutGrid.RowDefinitions[1].Height = e.NewValue;
            UpdateLabels();
        }

        private void UpdateLabels()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateLabels)}");

            TopLeftLabel.Text = $"Height: {(int)TopLeftLabel.Height}\nWidth: {(int)TopLeftLabel.Width}";
            TopRightLabel.Text = $"Height: {(int)TopRightLabel.Height}\nWidth: {(int)TopRightLabel.Width}";
            MiddleLeftLabel.Text = $"Height: {(int)MiddleLeftLabel.Height}\nWidth: {(int)MiddleLeftLabel.Width}";
            MiddleRightLabel.Text = $"Height: {(int)MiddleRightLabel.Height}\nWidth: {(int)MiddleRightLabel.Width}";
            BottomLeftLabel.Text = $"Height: {(int)BottomLeftLabel.Height}\nWidth: {(int)BottomLeftLabel.Width}";
            BottomRightLabel.Text = $"Height: {(int)BottomRightLabel.Height}\nWidth: {(int)BottomRightLabel.Width}";
        }
    }
}
