# Grid Sample #

This is a small bit of sample code demonstrating how to use a grid to organize a ContentPage into several regions in which you can place UI elements. Notice that the ContentPage has its background color set to black, but you do not see any black at all. This is because the grid fills the entire page.

The grid is spiced up a bit by allowing the size of the first (not the 0th, but the 1st) row to be dynamically changed based on the value of a slider.

Notice that the last two rows of the grid have their height set to 2\* and \*. This is called "star sizing", and sets up the grid so that the last row is always half the height of the second to last row.

Although the 1st row (the one that can be adjusted with the slider) is initially star-sized to 2*, as soon as you drag the slider around, the 1st row has its height set explicitly, and it will no longer be using star-sizing.

### Screenshots of Both Platforms
![Screenshot of this sample app running on both Android and iOS](GridDemoDroidIos.png "Cross-Platform Grid Sample")

### Questions? ###

* Always always always: If you have a question, someone else in class almost certainly has that same question, so please do ask!
* [Post questions on Cougar Courses](https://cc.csusm.edu/mod/oublog/view.php?id=630348)